import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/formInput1',
    name: 'Form Input 1',
    component: () => import('../views/FormInput1.vue')
  },
  {
    path: '/formInput2',
    name: 'Form Input 2',
    component: () => import('../views/FormInput2.vue')
  },
  {
    path: '/formInput3',
    name: 'Form Input 3',
    component: () => import('../views/FormInput3.vue')
  },
  {
    path: '/formInput4',
    name: 'Form Input 4',
    component: () => import('../views/FormInput4.vue')
  },
  {
    path: '/form1',
    name: 'Form 1',
    component: () => import('../views/Form1.vue')
  },
  {
    path: '/form2',
    name: 'Form 2',
    component: () => import('../views/Form2.vue')
  },
  {
    path: '/form3',
    name: 'Form 3',
    component: () => import('../views/Form3.vue')
  },
  {
    path: '/form4',
    name: 'Form 4',
    component: () => import('../views/Form4.vue')
  },
  {
    path: '/datatable1',
    name: 'Data table 1',
    component: () => import('../views/DataTable1.vue')
  },
  {
    path: '/productform1',
    name: '/Product Form 1',
    component: () => import('../views/ProductForm1.vue')
  },
  {
    path: '/productTable',
    name: '/Product Table',
    component: () => import('../views/product/ProductTable.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
